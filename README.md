A set of useful scripts for reversing golang binaries in IDA

# renamer.py
This script is used to restore function names in stripped go binary
All you need is just call one of following functions
```
   GoRename32()
   GoRename64()
```
# typeinfo.py
Script for restoring information about types used in binary.

Tested(poorly) at binaries compiled with go1.4, go1.5, go1.6, go1.7

For auto-recovering based on .typelink section need to call one of 
following functions (actually you can easily add same for 32 bits):
```
typesGo14_64()
typesGo15_64()
typesGo16_32()
typesGo16_64()
typesGo17_64()
```

Moreover you can use Go1XTypes for defining types which are not automatically defined
```
parser = Go1XTypes(0,0,step=Size[QD]word, standardTypes=False)
parser.handle_offset(<address_of_type>)
```
standardTypes param is used to avoid recreation of standard go structures.
If you doesn't use auto-recovering this param should be "True"
## Done

1. Recreating type structures from *.typeinfo* section
2. Creation of "kind" enum
3. Type definition based on "kind" field
4. Recursive type exploring from already defined types
5. Parsing structures fields used in binary
6. Add used structures at IDA structures list

## Will be done


2. Automatically definition of types used in *runtime_newobject* calls - partially implemented
3. Add PE files support (got an algo. Will be done for ZN2016 presentation)
4. Rewrite in order to simplify process of use(some forms buttons, etc.)

# Issues

Some issues with binaries compiled with go1.7 while it was in "nigthly". One of such
examples is *Go17_diff_uncommon* class. This class was created because some interesting
files (Linux.Rex) exist compiled with such nigthly go and "uncommonType" structure in this binary
has other format that in final go1.7 version.

My code is awful. I know. Sorry! :)

# Examples
Before
![Before](http://i.imgur.com/GXgPryO.png)
Methods implementation for ptr
![Methods implementation for ptr](http://imgur.com/fs7UbPM.png)
Structure parsing
![Structure parsing](http://imgur.com/9OSSdI3.png)
